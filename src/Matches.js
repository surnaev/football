import React from 'react'
import styled from 'styled-components'
import { useTable } from 'react-table'

import {
  useParams 
} from 'react-router-dom';

const Styles = styled.div`
  padding: 1rem;
  table {
    margin: auto;
    text-align: center;
    background-color: #ffffff;
    border-spacing: 0;
    border: 4px solid #99e6ff;
    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }
    th,
    td {
      margin: 0;
      padding: 0.5rem 5rem ;
      border-bottom: 3px solid #99e6ff;
      border-right: 3px solid #99e6ff;
      :last-child {
        border-right: 0;
        
      }
    }
  }
`

function Table({ columns, data }) {
  // Use the state and functions returned from useTable to build your UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data,
  })

  // Render the UI for your table
  return (
    <table {...getTableProps()}>
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()}>{column.render('Header')}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row)
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map(cell => {
                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
              })}
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

export default function Matches() {

  let {name, id} = useParams();  

  const [error, setError] = React.useState(null);
  const [isLoaded, setIsLoaded] = React.useState(false);
  const [items, setItems] = React.useState([]);

  React.useEffect(() => {
    fetch("https://api.football-data.org/v2/" + name + "/" + id + "/matches",
    {
        method:'GET', 
        headers: {'X-Auth-Token': process.env.REACT_APP_FOOTBALL_TOKEN}
    }
    ).then(res => res.json())
      .then(
        (result) => {
          setItems(result);
          setIsLoaded(true);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [id, name])
  
  const columns = React.useMemo(
    () => [
      {
        Header: 'Матчи',
        columns: [
          {
            Header: 'Хозяева ',
            accessor: 'homeTeam.name',
          },
          {
            Header: 'Счёт',
            accessor:'score.fullTime',
            Cell: ({value}) => { 
             if (value.homeTeam === null || value.awayTeam === null){
             return 'Нет данных';
             } else 
             return value.homeTeam + ' : ' + value.awayTeam
            
          }
          },
          {
            Header: 'Гости',
            accessor: 'awayTeam.name',
          },
        ],
      },
    ],
    []
  )

  if (error) {
      return <div>Ошибка: { error.message }</div>
  } else if (!isLoaded && items.length === 0) {
      return <div>Загрузка...</div>
  } else {

  return (
    <Styles>
        <Table columns={columns} data={items['matches']}/> 
    </Styles>
    )
  }
}
