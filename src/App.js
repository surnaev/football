import './App.css';
import Teams from './Teams';
import Competitions from './Competitions';
import Matches from './Matches';


import React from 'react';
import {
  HashRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';


export default function App() {
  return (
    <Router>
      <header>
      <h1>
        Футбол
      </h1>
      </header>
      <div>
        <Switch>
          <Route path="/competitions/:id/teams">
            <Teams />
          </Route>

          <Route path="/:name/:id/matches">
            <Matches />
          </Route>

          <Route path="/:name/:id/matches">
            <Matches />
          </Route>
          <Route path="/">
            <Competitions/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}




